# malakoff

Kikou

La demande  se trouve dans Scraping données B2B - CCN ACI_Syndics_copro.docx

On va utiliser :
des "jupyter notebook". c'est comme des fichiers python, mais en plus pratique : tu fais des bouts de script et tu peux les lancer un par un. 

une librairie qui s'appelle request pour se connecter aux sites et récupérer les données
une librairie python qui s'appelle Beautiful Soup pour trier et interpréter les données html
une librairie qui s'appelle pandas pour faire de jolis tableaux et les exporter dans excel

Dans le dossier exemples, il y a des exemples d'un script que j'ai fait avec la startup pour te donner la structure générale.

Et dans scrapping_SR.ipynb, c'est le début de la mission, pour te lancer dans la bonne voie ;) 

du coup, pour installer les bonnes librairies, il faudra faire surement quelque chose comme:

pip install requests numpy pandas beautifulsoup4

Voilou, on en reparle.

Let's go :)

Morgan